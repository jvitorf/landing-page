class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def landing
    @email = 'test@example.com'
    @prefs = { :mailing_list1=>{ :subscribed=>true, :name=>'Mailing List One', :desc=>'This is a longer description' }, :list2=>{ :subscribed=>false, :name=>"Mailing List Two", :desc=>"This, too, is a longer description" }, :newsletter1=>{ :subscribed=>true, :name=>"Our Weekly Newsletter", :desc=>"Hear all about some stuff.  Weekly." }}
  end
end
